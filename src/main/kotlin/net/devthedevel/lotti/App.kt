package net.devthedevel.lotti

import com.natpryce.konfig.Misconfiguration
import mu.KotlinLogging
import net.devthedevel.lotti.config.Config
import net.devthedevel.lotti.db.LotteryDatabase
import java.sql.SQLException
import kotlin.system.exitProcess

private val log = KotlinLogging.logger {}

private const val EXIT_MISCONFIG = 1;
private const val EXIT_INIT_ERROR = 2;
private const val EXIT_SQL_ERROR = 3;
private const val EXIT_THROWABLE = 4;

fun main(args: Array<String>) {
    try {
        log.info { "Initializing Lotti..." }

        log.info { "ARG TEST" }
        args.forEach {
            println("ARG: $it")
        }

        log.info { "Initializing configs..." }
        Config.init(args)
        log.info { "Configs initialized!" }

        //Init database
        log.info { "Initializing database tables..." }
        LotteryDatabase.initTables()
        log.info { "Database tables initialized!" }

        //Register Discord event listener
        log.info { "Registering event handler..." }
        Lotti.CLIENT.dispatcher.registerListener(LottiEventHandler())
        log.info { "Registered event handler!" }

        //Log bot into Discord
        log.info { "Logging into the Discord gateway..." }
        Lotti.CLIENT.login()
        log.info { "Logged into the Discord gateway!" }

        //All good
        log.info { "Lotti initialized!" }
    } catch (e: Misconfiguration) {
        log.error(e) { "Misconfiguration. Check your config" }
        exitProcess(EXIT_MISCONFIG)
    } catch (e: ExceptionInInitializerError) {
        log.error(e) { "Exception In Initializer" }
        exitProcess(EXIT_INIT_ERROR)
    } catch (e: SQLException) {
        log.error(e) { "SQL Exception" }
        exitProcess(EXIT_SQL_ERROR)
    } catch (e: Throwable) {
        log.error(e) { "Something bad happened..." }
        exitProcess(EXIT_THROWABLE)
    }

    //Register shutdown hook
    Runtime.getRuntime().addShutdownHook(Thread {
        fun run() {
            print("Shutting down Lotti...")

            if (Lotti.CLIENT.isLoggedIn) {
                Lotti.CLIENT.logout()
                log.info { "Lotti logged out" }
            }

            log.info { "Shutdown complete. Goodbye!" }
        }
    })
}