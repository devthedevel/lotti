FROM openjdk:8-jdk

RUN git clone --depth 1 https://gitlab.com/devthedevel/lotti.git

WORKDIR /lotti

RUN chmod +x gradlew
RUN ./gradlew installDist

WORKDIR /lotti/build/libs

ENTRYPOINT ["java", "-Djdbc.drivers=org.postgresql.Driver -jar net.devthedevel.lotti-1.0-SNAPSHOT.jar"]